# ProjektDieta

copy of my C# project (I did most of the backend, connection /w api, entity framework), my colleague worked on the visual side

## Name
Diet application

## Description
Application allowing you to search for various recipies with the use of spoonacular api. When you see a recipe you like you schedule it on a day of the week. Application stores all the info in an entity framework database. You can also display a nutrition summary of any week. Unfortunetly it doesn't work as intended because of how the API stores data (some recipes store nutrition info per portion, some contain info about whole meal :C )

## Project status
Despite few imperfections we turned the project in and got a 5. 
